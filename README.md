# How to get started

Install Docker

$ docker pull gw000/keras-full (4 GByte)

Im Verzeichnis mit dem Notebook:
$ docker run -d -p 8888:8888 -v $(pwd):/srv gw000/keras-full

Im Browser http://localhost:8888/ öffenen und Notebook ausführen (Passwort: keras)
